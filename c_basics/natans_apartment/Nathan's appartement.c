



// ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int* brute_forcer(int* arr, int length);
int* option_generator(char* code, const int length);
void print(int* arr, int length);

int* brute_forcer(int* arr, int length) {

	/*

	Brute force function that uses recursive logic to enable password cracking of any length.
	Uses the list generated earlier by "option_generator"

	*/

	int* output_arr = (int*)malloc((int)(pow(5,length)+1) * sizeof(int));
	int counter = 0;

	if (arr[6] == NULL) {
		for (int i = 0; i < 5; i++) {
			if (arr[i] != -1) {
				output_arr[counter] = arr[i];
				counter++;
			}
		}
		output_arr[counter] = NULL;
		return output_arr;
	}
	int* next_output = (int*)malloc((int)(pow(5, length) + 1) * sizeof(int));
	next_output = brute_forcer(arr + 5, length - 1);
	for (int i = 0; i < 5; i++) {
		if (arr[i] != -1) {
			for (int j = 0; j < pow(5, length) + 1; j++) {
				if (next_output[j] == NULL) {
					break;
				}

				// can be implemented with strcat() and string concatination

				output_arr[counter] = arr[i] * pow(10, (1 + floor(log10(next_output[j])))) + next_output[j];
				counter++;
			}
		}
	}
	output_arr[counter] = NULL;
	return output_arr;
}




int* option_generator(char* code, const int length) {

	/*
	
	Function that is used to generate all the possible combinations for every number.
	
	*/

	int* arr = (int*)malloc(((5*length)+1)*sizeof(int));

	for (int k = 0; k < length * 5; k++) {
		arr[k] = -1;
	}
	arr[length * 5 + 1] = NULL;
		
	int number = atoi(code);

	for (int i = 0; i < strlen(code); ++i) {
		int pos = 0;

		int value = number % 10;

		arr[i* 5 + pos] = value;

		pos++;

		if ((value + 3) >= 0 && (value + 3) <= 9) {

			arr[i * 5 + pos] = value + 3;
			pos++;

		}

		if ((value - 3) >= 1 && (value - 3) <= 9) {

			arr[i * 5 + pos] = value - 3;
			pos++;

		}


		if (value == 8) {
			arr[i * 5 + pos] = 0;
			pos++;
		}
		if (value == 0) {
			arr[i * 5 + pos] = 8;
			pos++;
		}
		int line = value % 3;

		if (line == 0 && value != 0) {

			arr[i * 5 + pos] = value - 1;
			pos++;

		}
		if (line == 1) {

			arr[i * 5 + pos] = value + 1;
			pos++;

		}
		if (line == 2) {

			arr[i * 5 + pos] = value + 1;
			pos++;
			arr[i * 5 + pos] = value - 1;
			pos++;

		}
		number = number / 10;


	}
	
	return arr;

}

void print(int * arr, int length)
{

	// Print function for the final output

	int i, j;
	for (i = 0; arr[i] != NULL; i++) {
		if (i % 10 == 0) {
			printf("\n");
		}
		printf("%d ", arr[i]);
	}
	
}

int main(int argc, char* argv[])
{
	char str[] = "1234"; // test string

	int len = strlen(str);

	printf("\n\n");

	print(brute_forcer(option_generator(str, len), len),len);
	
	return 0;
}
