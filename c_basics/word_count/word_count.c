/**
 * @file word_count.c
 * @version 1.0
 * @author ????????
 *
 * @brief program that print how many words are in a given file.
 *
 * @section LICENSE
 * This program is part of the Black-Biss;
 * ANY USE OF THE PROGRAM OR THE SOURCE CODE WITHOUT THE BLACK-BISS AGREEMENT IS FORBIDDEN.
 *
 * @section DESCRIPTION
 * The system get file path as argument. Open it and count the word's in it.
 * Input  : file_path
 * Output : print out the amount of words.
 */


 // ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int counter(char* file) {

	size_t word_count = 0;

	char ch;

	FILE* fptr;

	if ((fptr = fopen(file, "r")) == NULL) {
		printf("Error! opening file");
		exit(1);
	}

	char prev = "";

	ch = fgetc(fptr);
	while (ch != EOF)
	{
		if (!(ch >= 'A' && ch <= 'Z' || ch >= 'a' && ch <= 'z' || ch == 39) && (prev >= 'A' && prev <= 'Z' || prev >= 'a' && prev <= 'z' || prev == 39))
		{
			word_count++;
		}

		prev = ch;
		ch = fgetc(fptr);
	}

	fclose(fptr);

	return word_count;
}



int main(int argc, char* argv[])
{
	
	// you can add more initial checks
	if (argc < 2)
	{
		printf("missing file_path parameter.\n");
		return EXIT_FAILURE;
	}

	char * file= argv[1];

	
	printf("The file %s contain %u words.\n", file, counter(file));

	return 0;
}