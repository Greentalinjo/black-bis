/**
 * @file substrings.c
 * @version 1.0
 * @author ????????
 *
 * @brief program that check if one string is substring of the other and return pointer
 * to the start of the substring in the string.
 *
 * @section LICENSE
 * This program is part of the Black-Biss;
 * ANY USE OF THE PROGRAM OR THE SOURCE CODE WITHOUT THE BLACK-BISS AGREEMENT IS FORBIDDEN.
 *
 */

 // ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

// ------------------------------ functions -----------------------------

bool check_contains(const char* sub, const char* long_s) {

	// function that checks if longer string contains shorter string

	if (strstr(long_s, sub) != NULL) {
		return true;
	}

	return false;
}


char* my_strstr(const char* a, const char* b)
{	

	// function that acts as a wrapper to check_contains. finds shorter string out of the 2

	if (strlen(a) > strlen(b)) {
		if (check_contains(b, a) == true) {
			return b;
		}
	}
	if (strlen(a) < strlen(b)) {
		if (check_contains(a, b) == true) {
			return a;
		}
	}
	if (strlen(a) == strlen(b)) {
		if (strcmp(a, b) == 0) {
			return a;
		}
	}
	return NULL;
}

int main(int argc, char* argv[])
{
	char s1[] = "AAAAAAACCCAAAAAAAA";
	char s2[] = "CCC";

	char* ret = my_strstr(s1, s2);

	printf("The substring is: %s\n", ret);

	return 0;
}