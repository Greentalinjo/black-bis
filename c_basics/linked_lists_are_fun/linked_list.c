
 // ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node {
	int value;
	struct node* next;
} node_t;

void print_list(node_t* root) {

	// Print function for linked list

	node_t* current = root;

	while (current != NULL) {
		printf("%d\n", current->value);
		current = current->next;
	}
}

int insert(node_t** root, int pos) {

	/*
	Function for inserting node_t objects. implemented by making the ptr of previous node
	equal the new node and the new node's ptr the next node.
	if inserted in pos 0 will append it as the new root.
	Designed for use with unsigned integers, negative pos values will not produce a relevant result.
	*/

	int iterator = 0;

	if (pos == 0) {
		node_t* new = (node_t*)malloc(sizeof(node_t));
		new->value = 1337;
		new->next = *root;
		*root = new;
	}
	else {

		node_t* original = (node_t*)malloc(sizeof(node_t));

		original = (*root);

		while (iterator != pos - 1) {

			if ((*root)->next == NULL) {
				printf("Index out of bounds");
				exit(1);
			}
			(*root) = (*root)->next;
			iterator++;
		}

		node_t* temp = (node_t*)malloc(sizeof(node_t));
		temp->value = 1337;
		temp->next = (*root)->next;
		(*root)->next = temp;

		(*root) = original;

		return 0;
	}
	return 0;

}

int main(int argc, char* argv[])
{
	node_t* root = NULL;
	root = (node_t*)malloc(sizeof(node_t));
	root->value = 1;
	root->next = (node_t*)malloc(sizeof(node_t));
	root->next->value = 2;
	root->next->next = (node_t*)malloc(sizeof(node_t));
	root->next->next->value = 3;
	root->next->next->next = NULL;

	print_list(root);

	insert(&root, 2);

	print_list(root);

	return 0;
}