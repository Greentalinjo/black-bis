def tuple_creator(arr1, arr2):

    return list(zip(arr1, arr2))

def tuples_to_dict(tuple_list):

    return dict(tuple_list)

def array_to_dict(array1, array2):

    return tuples_to_dict(tuple_creator(array1,array2))

def main():

    ############### One ############### - implemented according to 3

    array1 = [1,2,3,4,5]
    array2 = [6,7,8,9,0]
    print(tuple_creator(array1, array2))

    ############### Two ############### - implemented according to 3

    print(tuples_to_dict(tuple_creator(array1, array2)))

    ############### Four ##############

    print(array_to_dict(array1, array2))

    ############### Five ############## - same code works as was used in the last assignment

    array1_1 = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
    array2_2 = ["hello","i'm","greental",9,0]

    print(array_to_dict(array1_1, array2_2))


if __name__ == '__main__':
    main()
