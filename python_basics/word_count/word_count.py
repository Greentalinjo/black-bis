import sys
import re

def text_parser(filename, rev, sort_param):
    file_txt = open(filename, 'r')

    joined_text = ''.join(file_txt.readlines())

    res_list = re.findall(r"[a-zA-Z0-9]+'?[a-zA-Z0-9]*", joined_text.lower()) 

    for result in range(len(res_list)):
        if res_list[result][-1] == chr(39) or res_list[result][-1] == chr(34):
            res_list[result] = res_list[result][:-1]

    output_dict = {}
    
    for word in res_list:
        if word in output_dict:
            output_dict[word]+=1
        else:
            output_dict[word] = 1
    
    sorted_dict = sorted(output_dict.items(), key=lambda x: x[sort_param], reverse=rev)

    return sorted_dict

def print_words(filename):
    
    sorted_dict = text_parser(filename, False, 0)

    for sorted_value in sorted_dict:
        print(sorted_value)

def print_top(filename):
    
    sorted_dict = text_parser(filename, True, 1)

    iterator = 0

    while iterator < 20:
        print(sorted_dict[iterator])
        iterator+=1
    
def main():
    if len(sys.argv) != 3:
        print("usage: ./wordcount.py {--count | --topcount} file")
        sys.exit(1)

    option = sys.argv[1]
    filename = sys.argv[2]
    if option == '--count':
        print_words(filename)
    elif option == '--topcount':
        print_top(filename)

    else:
        print("unknown option: " + option)
        sys.exit(1)


if __name__ == '__main__':
    main()
