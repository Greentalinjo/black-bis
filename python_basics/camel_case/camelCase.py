import re 
  
def camel_case_to_text(input_str): 
    
    # Simple regex to find out words in the input string

    return re.findall(r'[A-Z](?:[a-z]+|[A-Z]*(?=[A-Z]|$))', input_str) 
      

input_str = input(">>> ")
print(camel_case_to_text(input_str)) 