"""
Black Biss - Basic Python

Write a function that replace any second word with the word "Corona".
"""


def word_pandemic(in_array):

    output_arr = in_array.split()

    iterator = 0

    for word in output_arr:
        if iterator % 2 == 1:
            output_arr[iterator] = "Corona"
        iterator+=1
    
    return ' '.join(output_arr)

# small test to check it's work.
if __name__ == '__main__':
    base_word = "Koala Bears are the cutest"
    sick_word = "Koala Corona are Corona cutest"

    if word_pandemic(base_word) == sick_word:
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")
