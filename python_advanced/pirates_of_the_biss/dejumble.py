"""
Black Biss - Advance Python

Write a function that suggest which word is the Pirates meant to write.
"""


def dejumble(word, potentional_words):

    word_set = set(word)

    output_array = []

    for input_word in potentional_words:
        if set(input_word) == word_set:
            output_array.append(input_word)
    
    return output_array


# small test to check it's work.
if __name__ == '__main__':

    ret = dejumble("orspt", ["sport", "parrot", "ports", "matey"])
    if set(ret) == set(("sport", "ports")): # changed a few syntax errors that were included including running set on 2 strings and not on tuple
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")
