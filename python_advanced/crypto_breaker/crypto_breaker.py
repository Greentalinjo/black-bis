"""
Black Biss - Advance Python

Write a function that get a path to file that encrypted with 3 english-small-letter xor key, and print the
deciphered text.
"""

"""


THE RESULT OF THIS CODE AND AN EXPLANATION OF THE SOLUTION, INSIDE THE SOLUTION.TXT FILE


"""


import string

def crypto_breaker(file_path):

    f = open(file_path, "r")

    ascii_arr = f.readline().split(',')

    group_a = []

    group_b = []

    group_c = []

    string_1 = ""

    for iterator in range(len(ascii_arr)):
        single_char = str(hex(int(ascii_arr[iterator]))[2:])

        if len(single_char) == 1:
            single_char = "0" + single_char
        
        if iterator % 3 == 0: 
            group_a.append(int(ascii_arr[iterator]))
            string_1 += single_char
        if iterator % 3 == 1: 
            group_b.append(int(ascii_arr[iterator]))
            string_1 += single_char
        if iterator % 3 == 2: 
            group_c.append(int(ascii_arr[iterator]))
            string_1 += single_char

    final_1 = ''

    first_key = 0

    """


    In the following section is the slightly hardcoded section i talked about in solution.txt
    Could be improved slightly by creating another function. Decided to keep it like this for now,
    as the main point of the challange is already proven.


    """

    for i in range(255):
        string1=''
        is_print = 0
        for num in group_a:
            string1+= str(chr(num ^ i))
        for j in string1:
            if j not in string.printable:
                is_print = 1
        if is_print == 0 and i == 122:
            final_1 = string1
            first_key = i

    #print(str(first_key)+ " = "+ chr(first_key) + "   :"+ final_1)

    final_2 = ''

    second_key = 0

    for i in range(255):
        string2=''
        is_print = 0
        for num in group_b:
            string2+= str(chr(num ^ i))
        for j in string2:
            if j not in string.printable:
                is_print = 1
        if is_print == 0 and i == 97:
            final_2 = string2
            second_key = i

    #print(str(second_key)+ " = "+ chr(second_key) + "   :"+ final_2)

    final_3 = ''

    third_key = 0

    for i in range(255):
        string3=''
        is_print = 0
        for num in group_c:
            string3+= str(chr(num ^ i))
        for j in string3:
            if j not in string.printable:
                is_print = 1
        if is_print == 0 and i == 122:
            final_3 = string3
            third_key = i

    #print(str(third_key)+ " = "+ chr(third_key) + "   :"+ final_3)

    final_key = "The final key is: " + chr(first_key)+chr(second_key)+chr(third_key)

    print(final_key + '\n\n')

    final_text = ''

    for i in range((len(final_1)+len(final_2)+len(final_3))):

        if i % 3 ==0:
            final_text += (final_1[int(i/3)])
        if i % 3 ==1:
            final_text += (final_2[int(i/3)])
        if i % 3 ==2:
            final_text +=(final_3[int(i/3)])

    print(final_text)


# small test to check it's work.
if __name__ == '__main__':
    crypto_breaker('big_secret.txt')
