"""
Black Biss - Advance Python

Write a function that get array of string, each represent a row in 2-dim map, when:
    'X' is start / destination point
    '-' is road right-left
    '|' is road up-down
    '+' is a turn in the road, can be any turn (which isn't continue straight)
    path = ["           ",
            "X-----+    "
            "      |    "
            "      +---X",
            "           "]

    # Note: this grid is only valid when starting on the right-hand X, but still considered valid
    path = ["                      ",
            "   +-------+          ",
            "   |      +++---+     ",
            "X--+      +-+   X     "]
    nevigate_validate(path)  # ---> True
"""

# Array of indexes traveled and success flag 

list1 = []

success_flag = 0

def nevigate_validate(path):

    # Main path-finding function
    
    global list1
    global success_flag

    x_arr = []
    
    for string_i in range(len(path)):
        for line_i in range(len(path[string_i])):
            if path[string_i][line_i] == "X":
                x_arr.append([line_i,string_i])

    if(len(x_arr) != 2):
        return False

    start0 = x_arr[0]
    start1 = x_arr[1]

    direction0 = find_direction(path, start0)
    direction1 = find_direction(path, start1)
    
    list1 = []
    success_flag = 0

    walk(path, start0, direction0[0], direction0[1])
    if(success_flag == 1):
        return True

    list1 = []
    success_flag = 0

    walk(path, start1, direction1[0], direction1[1])
    if(success_flag == 1):
        return True

    return False

def find_direction(path, pos):

    # Function used to find the initial starting direction of the path

    try:
        if(path[pos[1]][pos[0]+1] != ' '):
            return (["horizontal", "right"])
    except:
        pass
    try:
        if(path[pos[1]][pos[0]-1] != ' '):
            return (["horizontal", "left"])
    except:
        pass
    try:
        if(path[pos[1]+1][pos[0]] != ' '):
            return (["vertical", "down"])
    except:
        pass
    try:
        if(path[pos[1]-1][pos[0]] != ' '):
            return (["vertical", "up"])
    except:
        pass


def walk(pos, start, direction, left_right_up_down):

    # Function designed to walk in streight parts of the path

    global list1
    global success_flag

    if (direction == "horizontal"):
        symbol = "-"
    if (direction == "vertical"):
        symbol = "|"
    if(left_right_up_down == "right"):
        while(pos[start[1]][start[0]+1] == symbol):
            start[0]+=1
        start[0]+=1
        if(pos[start[1]][start[0]] == "|"):
            return False
        if(pos[start[1]][start[0]] == "+" and [start[1],start[0]] not in list1):
            list1.append([start[1],start[0]])            
            turn(pos, start[1], start[0], "right")
        if(pos[start[1]][start[0]] == "X"):
            success_flag = 1
    if(left_right_up_down == "left"):
        while(pos[start[1]][start[0]-1] == symbol):
            start[0]-=1
        start[0]-=1
        if(pos[start[1]][start[0]] == "|"):
            return False
        if(pos[start[1]][start[0]] == "+" and [start[1],start[0]] not in list1):
            list1.append([start[1],start[0]])            
            turn(pos, start[1], start[0], "left")
        if(pos[start[1]][start[0]] == "X"):
            success_flag = 1
    if(left_right_up_down == "down"):
        while(pos[start[1]+1][start[0]] == symbol):
            start[1]+=2
        start[1]+=1   
        if(pos[start[1]][start[0]] == "-"):
            return False
        if(pos[start[1]][start[0]] == "+" and [start[1],start[0]] not in list1):
            list1.append([start[1],start[0]])            
            turn(pos, start[1], start[0], "down")
        if(pos[start[1]][start[0]] == "X"):
            success_flag = 1
    if(left_right_up_down == "up"):
        while(pos[start[1]-1][start[0]] == symbol):
            start[1]-=1
        start[1]-=1   
        if(pos[start[1]][start[0]] == "-"):
            return False
        if(pos[start[1]][start[0]] == "+" and [start[1],start[0]] not in list1):
            list1.append([start[1],start[0]])            
            turn(pos, start[1], start[0], "up")
        if(pos[start[1]][start[0]] == "X"):
            success_flag = 1

def turn(path, starty, startx, starting_dir):

    # Function designed to handle turns

    if(starting_dir == "right" or starting_dir == "left"):
        if(starty+1 < len(path)):
            if(starty-1 >= 0):
                if(path[starty+1][startx] == '|' and path[starty-1][startx] not in ['+', '|']):
                    walk(path, [startx, starty+1], "vertical", "down")
            else:
                if(path[starty+1][startx] == '|'):
                    walk(path, [startx, starty+1], "vertical", "down")
        if(starty-1 >= 0):
            if(starty+1 < len(path)):
                if(path[starty-1][startx] == '|' and path[starty+1][startx] not in ['+', '|']):
                    walk(path, [startx, starty-1], "vertical", "up")
            else:
                if(path[starty-1][startx] == '|'):
                    walk(path, [startx, starty-1], "vertical", "up")

        if(starty-1 >= 0):
            if(starty+1 < len(path)):
                if(path[starty-1][startx] == '+' and path[starty+1][startx] not in ['+', '|']):
                    turn(path, starty-1, startx, "up")
            else:
                if(path[starty-1][startx] == '+'):
                    turn(path, starty-1, startx, "up")   
        if(starty+1 < len(path)):
            if(starty-1 >= 0):
                if(path[starty+1][startx] == '+' and path[starty-1][startx] not in ['+', '|']):
                    turn(path, starty+1, startx, "down")
            else:
                if(path[starty+1][startx] == '+'):
                    turn(path, starty+1, startx, "down")
                
    if(starting_dir == "up" or starting_dir == "down"):
        if(startx+1 < len(path[0])):
            if(startx-1 >= 0):
                if(path[starty][startx+1] == '-' and path[starty][startx-1] not in ['+', '-']):
                    walk(path, [startx+1,starty], "horizontal", "right")
            else:
                if(path[starty][startx+1] == '-'):
                    walk(path, [startx+1,starty], "horizontal", "right")
        if(startx-1 >= 0):
            if(startx+1 < len(path[0])):
                if(path[starty][startx-1] == '-' and path[starty][startx+1] not in ['+', '-']):
                    walk(path, [startx-1,starty], "horizontal", "left")
            else:
                if(path[starty][startx-1] == '-'):
                    walk(path, [startx-1,starty], "horizontal", "left")
        if(startx-1 >= 0):
            if(startx+1 < len(path[0])):
                if(path[starty][startx-1] == '+' and path[starty][startx+1] not in ['+', '-']):
                    turn(path, starty, startx-1, "left")
            else:
                if(path[starty][startx-1] == '+'):
                    turn(path, starty, startx-1, "left")
        if(startx+1 < len(path[0])):
            if(startx-1 >= 0):
                if(path[starty][startx+1] == '+' and path[starty][startx-1] not in ['+', '-']):
                    turn(path, starty, startx+1, "right")
            else:
                if(path[starty][startx+1] == '+'):
                    turn(path, starty, startx+1, "right")
                    
        if(path[starty][startx-1] == '+' and path[starty][startx+1] not in ['+', '-']):
            turn(path, starty, startx-1, "left")

# small test to check it's work.
if __name__ == '__main__':
    path = ["           ",
            "X-----+    ",
            "      |    ",
            "      +---X",
            "           "]
    valid = nevigate_validate(path)
    
    path = ["           ",
            "X--|--+    ",
            "      -    ",
            "      +---X",
            "           "]

    invalid = nevigate_validate(path)

    #path = ["                      ",
    #        "   +-------+          ",
    #        "   |      +++---+     ",
    #        "X--+      +-+   X     "]
    #
    #print(nevigate_validate(path))
    
    if valid and not invalid:
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")
