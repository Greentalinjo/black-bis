from greenery import fsm
from greenery import lego

def baseB(n):

    # Convert input into binary string

    return str(bin(n)[2:])

def divided_by_N(number):
    
    # Generates a DFA to check divisibility by input number

    ACCEPTING_STATE = START_STATE = '0'
    SYMBOL_0 = '0'
    dfa = { 
        str(from_state): {
            str(symbol): 'to_state' for symbol in range(2)
        }
        for from_state in range(int(number))
    }
    dfa[START_STATE][SYMBOL_0] = ACCEPTING_STATE
    lookup_table = { SYMBOL_0: ACCEPTING_STATE }.setdefault
    for num in range(int(number) * 2):
        end_state = str(num % int(number))
        num_s = baseB(num) #turn number to binary
        before_end_state = lookup_table(num_s[:-1], START_STATE)
        dfa[before_end_state][num_s[-1]] = end_state
        lookup_table(num_s, end_state)
    return dfa

def num_of_states(dfa):

    # Returns number of states in the DFA 

    return(len(dfa))

def states(num):

    # Returns set of DSA states

    string = []
    for i in range(num):
        string.append(str(i))
    return set(string)

def dfa_to_regex(number):

    # Function creates a dfa, and then converts it into regex

    dfa = divided_by_N(number)
    states_num = num_of_states(dfa)
    automata = fsm.fsm(
        alphabet = {"0", "1"},
        states   = states(states_num),
        initial  = "0",
        finals   = {"0"},
        map      = dfa)
    return(lego.from_fsm(automata))

if __name__ == "__main__":

    # Main function

    number = input ("Enter NUMBER: ")
    final_regex = dfa_to_regex(number)
    print(final_regex)
