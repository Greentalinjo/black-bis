"""
Black Biss - Advance Python

Write a function that calculate how many month one should save money to buy new car.
"""


def home_economics(startPriceOld, startPriceNew, savingPerMonth, percentLossByMonth):
    
    if startPriceNew <= startPriceOld:
        return (0, startPriceOld-startPriceNew)
    
    total_cash = startPriceOld
    total_cost = startPriceNew

    car1_price = startPriceOld
    car2_price = startPriceNew

    #decreasing percentage once, as it will instantly be brought back up to 1.5 in the first loop iteration

    percentLossByMonth = percentLossByMonth - 0.5

    iterator = 0
    
    while total_cost > total_cash:
        if iterator % 2 == 0:
            print("percentage increased")
            percentLossByMonth += 0.5
        
        total_cash += savingPerMonth

        total_cash -= ((percentLossByMonth/100) * car1_price)
        total_cost -= ((percentLossByMonth/100) * car2_price)

        car1_price = car1_price - (car1_price * (percentLossByMonth/100))
        car2_price = car2_price - (car2_price * (percentLossByMonth/100))

        print ("total cost: " + str(total_cost))
        print ("total cash: " + str(total_cash))

        iterator+=1

    print((iterator, total_cash-total_cost))
    return (iterator, total_cash-total_cost)


# small test to check it's work.
if __name__ == '__main__':

    ret = home_economics(2000, 8000, 1000, 1.5)
    if ret[0] == 2 and ret[1] == 766:
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")
