"""
Black Biss - Advance Python

Write a function that calculate how far is our ejected pilot.
Run this program and insert the following lines (each end with Enter):

UP 5
DOWN 3
LEFT 3
RIGHT 2
0

and the result should be 2
"""
import sys

import math

def input_func():

    print("Usage: {UP | DOWN | RIGHT | LEFT} {NUMBER}\nTo finish the input, insert a 0")

    end_of_input = 1

    input_array = []

    while end_of_input != 0:

        user_input = input(">>> ").upper()
        
        input_check = user_input.split(" ")
        
        if len(input_check) == 2:
            if input_check[0] in ("LEFT", "RIGHT", "UP", "DOWN") and input_check[1].isdigit() :
                input_array.append(user_input)
            else:
                sys.exit("Bad input")     
        
        elif len(input_check) == 1:
            if input_check[0] == "0":
                end_of_input = 0
            else:
                sys.exit("Bad input")

        else:
            sys.exit("Bad input")

    print("User entered: " + str(input_array))

    return(input_array)

def ejected(input_array):
    
    vertical = 0
    horizontal = 0

    for instruction in input_array:

        vector = instruction.split(" ")

        if vector[0] == "DOWN":
            vertical -= int(vector[1])
        if vector[0] == "UP":
            vertical += int(vector[1])
        if vector[0] == "RIGHT":
            horizontal += int(vector[1])
        if vector[0] == "LEFT":
            horizontal -= int(vector[1])
    
    dist = round(math.sqrt(vertical**2 + horizontal**2))

    return dist


# small test to check it's work.
if __name__ == '__main__':
               
    print("Rounded dist: " + str(ejected(input_func())))
