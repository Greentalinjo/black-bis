"""
Black Biss - Advance Python

Write a function that read one line from the user with many potentional passwords seperated by comma ","
and print out only the valid password - by the following rules:
    * contain letter from each of the characters-sets:
        lower-case english, upper-case english, decimal digit,
        special characters *&@#$%^
    * dosn't contain other symbols (ignore white-spaces)
    * length between 6 and 12

"""
import re

def validate_passwords(passwords):
    
    res_list = []
    
    i = 0

    while i < len(passwords):
        res_list += re.findall(r"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,12}$", passwords[i])
        i+=1
    
    print(res_list)

# small test to check it's work.
if __name__ == '__main__':

    pass_input = input(">>>  ")

    pass_list = pass_input.split(',')
    
    validate_passwords(pass_list)
