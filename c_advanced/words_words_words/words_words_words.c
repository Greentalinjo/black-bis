

// ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// ------------------------------ functions -----------------------------

const char digits[9][10] = { "one","two","three","four","five","six","seven","eight","nine" };

const char special[10][10] = { "ten","eleven","twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","nineteen" };

const char tens[8][10] = { "twenty","thirty","forty","fifty","sixty","seventy","eighty","ninety" };

int one_to_ninety_nine() {

	// Sum digit lengths

	int digitsum = 0;

	for (int i = 0; i < 9; ++i) {
		digitsum += strlen(digits[i]);
	}

	// Sum special lengths
	
	int specialsum = 0;

	for (int i = 0; i < 10; ++i)
	{
		specialsum += strlen(special[i]);
	}

	// Sum 20+ lengths

	int tens_sum = 0;

	for (int i = 0; i < 8; ++i)
	{
		tens_sum += strlen(tens[i]) * 10 + digitsum;
	}

	// return the sum of all previous calculations, together with the length of "one hundred"

	return tens_sum+specialsum+digitsum+strlen("onehundred");
}



int main(int argc, char* argv[])
{

	// Print the output: 864.

	printf("%d", one_to_ninety_nine());

	return(0);
}