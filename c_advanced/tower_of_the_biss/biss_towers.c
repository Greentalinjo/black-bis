


 // ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* https://he.wikipedia.org/wiki/%D7%9E%D7%92%D7%93%D7%9C%D7%99_%D7%94%D7%90%D7%A0%D7%95%D7%99  

solution done in accordance with the recursive algorithm presented on wikipedia.

*/

int biss_towers(int disk_num, char src, char dst, char extra) {

	if (disk_num != 0) {
		biss_towers(disk_num - 1, src, extra, dst);
		printf("Move disk %d from %c to %c\n", disk_num, src, dst);
		biss_towers(disk_num - 1, extra, dst, src);
	}
	return 0;
}

int main(int argc, char* argv[])
{
	biss_towers(3, 65, 67, 66); // 65 - "A", 67 - "C", 66 - "B" 

	return 0;
}

