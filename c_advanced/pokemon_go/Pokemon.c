// ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Headers.h"


/*
EFFECTIVENESS:

EARTH - "E" 69

AIR - "A" 65

FIRE - "F" 70

WATER - "W" 87

*/

int main(int argc, char* argv[])
{

	pokemon* poke1 = (pokemon*)malloc(sizeof(pokemon));
	poke1->damage = 5;
	poke1->defense = 10;
	poke1->effectiveness = 69; // Earth
	pokemon* poke2 = (pokemon*)malloc(sizeof(pokemon));
	poke2->damage = 10;
	poke2->defense = 10;
	poke2->effectiveness = 87; // Water

	/*
		Expected winner is pokemon 1, with a power ranking of 75 against the 50 of his pokemon 2 opponent
	*/

	winner_pokemon(poke1, poke2);

	return 0;
}

void winner_pokemon(pokemon* p1, pokemon* p2) {
	char element1 = p1->effectiveness;
	char element2 = p2->effectiveness;

	int multiplier1, multiplier2;

	float p1power, p2power;

    // default condition if both elements are the same

    	multiplier1 = 2;
	multiplier2 = 2;

	// checking the conditions for effectiveness

	if (element1 == 87 && element2 == 70) {
		multiplier1 = 3;
		multiplier2 = 1;
	}
	if (element2 == 87 && element1 == 70) {
		multiplier1 = 1;
		multiplier2 = 3;
	}
	if (element1 == 70 && element2 == 65) {
		multiplier1 = 3;
		multiplier2 = 1;
	}
	if (element2 == 70 && element1 == 65) {
		multiplier1 = 1;
		multiplier2 = 3;
		
	}
	if (element1 == 69 && element2 == 87) {
		multiplier1 = 3;
		multiplier2 = 1;
	}
	if (element2 == 69 && element1 == 87) {
		multiplier1 = 1;
		multiplier2 = 3;
	}
	if (element1 == 65 && element2 == 69) {
		multiplier1 = 3;
		multiplier2 = 1;
	}
	if (element2 == 65 && element1 == 69) {
		multiplier1 = 1;
		multiplier2 = 3;
	}

    //redundant checks, due to both multipliers being 2 by default

    /*
	if ((element1 == 65 && element2 == 87) || (element2 == 65 && element1 == 87)) {
		multiplier1 = 2;
		multiplier2 = 2;
	}
	if ((element1 == 70 && element2 == 69) || (element2 == 70 && element1 == 69)) {
		multiplier1 = 2;
		multiplier2 = 2;		
	}
    */

	// calculation of pokemon power and comparison

	p1power = ((p1->damage) / (p2->defense)) * multiplier1 * 50;
	p2power = ((p2->damage) / (p1->defense)) * multiplier2 * 50;

	if (p1power > p2power) {
		printf("Pokemon 1 wins!");
	}

	if (p2power > p1power) {
		printf("Pokemon 2 wins!");
	}

	if (p1power == p2power) {
		printf("No winner here, it's a draw!");
	}

}