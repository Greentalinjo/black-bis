// ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Includes.h"


int main(int argc, char* argv[])
{

	rectangle* rect1 = (rectangle*)malloc(sizeof(rectangle));
	rect1->x = 420;
	rect1->y = 69;

	char* output = rect2sqr(rect1);

	printf("%s", output);

	return 0;
}

char* rect2sqr(rectangle* rect2) {

	// Wrapper function for unpacking a rectange object for easier later implementation

	return(func(rect2->x, rect2->y));
}

char* func(int num1, int num2) {

	// Recursive function for finding sqr in rect. reduces large side by small side every recursive call
	// and adds the small side to the sqr dimensions printing.

	int big, small, alloc_size;

	char* temp;

	if (num1 == 0 || num2 == 0) {
		return("");
	}

	if (num1 == num2) {

		alloc_size = size_of(num1)+1;

		char* buffer = (char*)malloc((alloc_size) * sizeof(char));
		snprintf(buffer, alloc_size, "%d", num1);
		return buffer;

	}

	if (num1 > num2) {

		big = num1;
		small = num2;

	}

	if (num1 < num2) {

		big = num2;
		small = num1;

	}

	temp = func(big - small, small);

	alloc_size = size_of(small) + 3 + strlen(temp);

	char* buffer = (char*)malloc((alloc_size) * sizeof(char));

	snprintf(buffer, alloc_size, "%d, ", small);

	return strcat(buffer, temp);

}

int size_of(int x) {
	int size = 0;

	while (x != 0) {
		++size;
		x = x / 10;
	}
	return size;
}
