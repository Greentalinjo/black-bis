
// ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// ------------------------------ functions -----------------------------

unsigned char mem[1000] = { 0 };

unsigned char* ptr = mem;

unsigned int pos = 0;

int bf_interpreter(char* file) {

	/*
		Complete brainfuck interpreter, supporting all the possible commands and loops.
		Supports nested loops.
		Expects proper brainfuck input, that is correct in terms of syntax.
	*/

	FILE* fptr;

	char c;

	if ((fptr = fopen(file, "r")) == NULL) {
		printf("Error! opening file");
		exit(1);
	}

	c = fgetc(fptr);
	while (c != EOF) {
		switch (c) {
		case '+':
			++ * ptr;
			break;
		case '-':
			-- * ptr;
			break;
		case '>':
			if (pos <= 998) {
				++ptr;
				++pos;
			}
			else {
				exit(-1);
			}
			break;
		case '<':
			if (pos >= 1) {
				--ptr;
				--pos;
			}
			else {
				exit(-1);
			}
			break;
		case '.':
			putchar(*ptr);
			break;
		case '[':
			if (*ptr == 0) {
				int count = 0;
				c = fgetc(fptr);
				while (1) {
					if (c == ']' && count == 0) {
						break;
					}
					else {
						if (c == '[') {
							count++;
						}
						if (c == ']') {
							count--;
						}
					}
					c = fgetc(fptr);
				}
			}
			break;
		case ']':
			if (*ptr != 0) {

				int count2 = 0;
				fseek(fptr, -2, SEEK_CUR);
				c = fgetc(fptr);
				while (1) {
					if (c == '[' && count2 == 0) {
						break;
					}
					else {
						if (c == ']') {
							count2++;
						}
						if (c == '[') {
							count2--;
						}
					}
					fseek(fptr, -2, SEEK_CUR);
					c = fgetc(fptr);
				}
			}
			break;

		}
		c = fgetc(fptr);
	}


}




int main(int argc, char* argv[])
{

	if (argc < 2)
	{
		printf("missing file_path parameter.\n");
		return EXIT_FAILURE;
	}

	char* file = argv[1];

	bf_interpreter(file);

	return(0);
}
